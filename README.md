# Project Image Hosting, and image modification

## Install

* sudo docker-compose build

## Run
* sudo docker-compose up

and go on : http://localhost:3000/

## Stop
* sudo docker stop $(sudo docker ps -q)

# Backend feature

* store a file
post : /upload

* get image
get : /download/:url

* api route for applied filter and text
```
parameter : 
:url : image url on the server
:modif : modification information

route:
post : /filter/canva/:url/:modif
```

* applied filter : view filter : https://gitlab.com/d0m00re/picturestore/-/wikis/All-type-of-filter

```
exemple : f_neg
pattern : f_[nameFilter]
```

```
neg : negatif filter
blue : blue filter-> keep only blue component (rgb)
red : red filter -> keep only red component (rgb)
sepia : sepia filter
gaussianBlur : gaussian blue filter
gaussianBlur2 : gaussian blue 2 filter
edge1 : edge1 filter
edge2 : edge2 filter
edge3 : edge3 filter
sharpen : sharpen filter
boxBlur : boxBlur filter
unsharpMasking : unsharpMasking filter
```


* add text

```
exemple : t_coucou toi,x_100,y_100,size_30,color_ffb200
pattern : t_[name text],x_[pos x],y_[pos y],color_[color]
```

```
t_ : identifier for the text
x_ and y_ : position on the image
size : size of the police
color : color text
```

* check argument parsing
```
/checkParsing/:url/:modif
```


# Front

* implement hosting and image modification