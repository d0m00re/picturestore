import React, { Component } from 'react';
import './App.css';

import UploadFile from './Component/UploadFile'
import AppliedEffect from './Component/AppliedEffect'


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";



//return <h1>Bonjour, {props.name}</h1>;

function DisImg(props) {
  return (
    <div>
      <img className={props.class}
        src={props.src}
        alt={props.alt}
      />
    </div>
  )
}

function TestOpt(props) {
  const urlImg = "2020-5-13-1589333400909_database_caagru.png"
  const dim = "w_400|h_400"
  const apiUrl = "http://localhost:3111/img/filter/canva"
  const allFilter = ["f_neg", "f_blue", "f_red", "f_sepia",
    "gaussianBlur", "gaussianBlur2", "edge1",
    "edge2", "edge3", "sharpen", "boxBlur", "unsharpMasking"]
  return (
    <div>

      {allFilter.map(name => <p> {name}
        <DisImg class="fit-picture" alt={name}
          src={`${apiUrl}/${urlImg}/${dim}|f_${name}`} />
      </p>)}

      <DisImg class="fit-picture"
        src={`${apiUrl}/${urlImg}/${dim}`}
        alt="No filter test" />

    </div>
  )
}

class ImageStoring extends Component {
  constructor(props) {
    super(props)

    this.state = {
      url: null,
      agument : ''
    }
  }

  // use for update the url
  setUrl = (elem) => {
    console.log('SET URL')
    console.log(elem)
    this.setState({ url: elem.url })
  }

  handleArgument = (event) => {
    console.log(event.target.value)
    this.setState({argument : event.target.value})
  }

  render() {
    return (
      <div>
        {this.state.url == null &&
          <UploadFile setUrl={this.setUrl} />
        }
        {this.state.url &&
          <div>
          <img src={'http://localhost:3111/img/filter/canva/' + this.state.url} />
          <img src={'http://localhost:3111/img/filter/canva/' + this.state.url + '/' + this.state.argument} />
          <h4>
            <label>Add argument :</label> 
            <input type="text" onChange={this.handleArgument}></input>
          </h4>
          </div>
        }
      </div>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props)
  }


  render() {
    return (
      <div className="App">
        <Router>
        <nav>
              <Link to="/">Upload File</Link>
              <Link to="/appliedEffect/Nop">Applied Effect</Link>
        </nav>

        <Switch>
          <Route exact path="/">
            <UploadFile />
          </Route>
          <Route path="/appliedEffect/:urlServerImg" component={AppliedEffect} />
          <Route component={() => <Redirect to='/' />} />
        </Switch>
        </Router>
      </div>
    );
  }
}
export default App;
