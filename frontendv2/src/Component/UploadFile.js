
import axios from 'axios';

import React, { Component } from 'react';

import {Redirect } from "react-router-dom";

// selecte file : update when an user upload a file
// imagePreviewUrl : use for preview our current image
class UploadFile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedFile: null,
      imagePreviewUrl: null,
      urlImg: null,
    };
  }

  // use when image is update
  onFileChange = event => {
    this.setState({ selectedFile: event.target.files[0] });

    let reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imagePreviewUrl: reader.result
      })
    }
    reader.readAsDataURL(event.target.files[0])
  };

  // On file upload (click the upload button)
  // use for upload the file on a server
  onFileUpload = (setUrl) => {

    // Create an object of formData 
    const formData = new FormData();

    // Update the formData object 
    formData.append(
      "myFile",
      this.state.selectedFile,
      this.state.selectedFile.name
    );

    // Details of the uploaded file 
    console.log(this.state.selectedFile);

    // Request made to the backend api 
    // Send formData object 
    var self = this;

    axios.post("http://localhost:3111/img/upload", formData)
      .then(function (response) {
        // updatate father state
        console.log('------------')
        console.log(response)
        console.log(response.data.url)
        self.setState({urlImg : response.data.url})
        //setUrl({ url: response.data.url, error: false })
      })
      .catch(function (err){
        console.log('putain de merde')
        console.log(err)
      })
  };

  // File content to be displayed after 
  // file upload is complete 

  fileData = () => {

    if (this.state.selectedFile && this.state.urlImg == null) {
      return (
        <div>
          <h2>PREVIEW IMAGE</h2>
          <img src={this.state.imagePreviewUrl} />
          <br />
          {this.state.selectedFile &&
            <button onClick={() => this.onFileUpload(this.props.setUrl)}>
              Upload!
          </button>
          }
        </div>
      );
        }
    else if(this.state.urlImg)
    {
      return (<Redirect to = {`/appliedEffect/${this.state.urlImg}`} />)

    }
    else {
      return (
        <div>
          <h2>UPLOAD A FILE</h2>
          <input type="file" onChange={this.onFileChange} />
        </div>
      );
    }
  };

  render() {
    return (
      <div>
        {
          this.fileData()
        }
      </div>
    );
  }
}

export default UploadFile; 