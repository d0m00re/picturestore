import React, { Component } from 'react'
import {Redirect } from "react-router-dom";
import './AppliedEffect.css';

const allFilter = ["neg", "blue", "red", "sepia",
    "gaussianBlur", "gaussianBlur2", "edge1",
    "edge2", "edge3", "sharpen", "boxBlur", "unsharpMasking"];

const fieldSeparator = '|';
const filterSeparator = 'f_';
const widthSeparator = 'w_';
const heightSeparator = 'h_';
const textSeparator = 't_';

const apiRootPath = 'http://localhost:3111/img/filter/canva/'

const generateDimString = (width, height) => {
    const dimString = widthSeparator + width + '|' + heightSeparator + height;

    return (dimString);
}

////t_john,x_100,y_100,size_0.0,color_black
const generateTextString = (text, posX, posY, size, color) => {
    console.log('text string')
    const textString = `${textSeparator}${text},x_${posX},y_${posY},size_${size},color_${color}`;

    return (textString);
}



export default class AppliedEffect extends Component {
    constructor(props) {
        super(props);

        let urlServerImg = props.match.params.urlServerImg
        let redirect = false

        if (urlServerImg.length < 10)
        {
            urlServerImg = prompt('Put Image Server Url :');
            if(urlServerImg.length < 10)
            {
                alert('Wrong Url File')
                this.props.history.push('/')
            }
            else
            {
                this.props.history.push(`/AppliedEffect/${urlServerImg}`)
            }
        }
        

        this.state = {
            baseUrl: urlServerImg,
            imageUrl: apiRootPath + urlServerImg,
            fullString: apiRootPath + urlServerImg,
            argument: '',

            widthImg: 0,
            heightImg: 0,

            text: "",
            textPosX: "1",
            textPosY: "1",
            textSize: "30",
            textColor: "#e66465",
        }
    }

    /*
    ** text component
    */

    handleText = (event) => {
        this.setState({ text: event.target.value })
    }

    handleTextPosX = (event) => {
        console.log('update')
        this.setState({ textPosX: event.target.value })
    }

    handleTextPosY = (event) => {
        console.log('update')
        this.setState({ textPosY: event.target.value })
    }

    handleTextSize = (event) => {
        this.setState({ textSize: event.target.value })
    }

    handleTextColor = (event) => {
        console.log(event.target.value)
        this.setState({ textColor: event.target.value })
    }

    handleTextAdd = (event) => {
        this.setState({ text: '' })
    }

    handleWidthImg = (event) => {
        this.setState({widthImg : event.target.value})
    }

    handleOnblurDimImg = () => {
        this.setState({ fullString: this.generateImgLinkEffect()})
    }

    handleHeightImg = (event) => {
        this.setState({heightImg : event.target.value})
    }

    /*
    ** generate new path with image effect
    */
    generateImgLinkEffect = (argument = null) => {
        let fullString = '';

        fullString = this.state.imageUrl + '/' + generateDimString(this.state.widthImg, this.state.heightImg) + fieldSeparator
        if (fullString)
            fullString = fullString + argument
        return (fullString)
    }

    addText = () => {
        console.log('Add text :')
        let stringAdd = this.state.argument;
        if (stringAdd != '')
            stringAdd += fieldSeparator;
        stringAdd += generateTextString(this.state.text, this.state.textPosX, this.state.textPosY, this.state.textSize, this.state.textColor.substring(1));
        //stringAdd += (filterSeparator + name);
        console.log(stringAdd)
        this.setState({ argument: stringAdd })
        this.setState({ fullString: this.generateImgLinkEffect(stringAdd) })
    }

    updateFullString = () => {
       this.setState({fullString : this.generateImgLinkEffect()})
    }

    /*
    ** add filter
    */
    addFilter = (name) => {
        console.log(`Add ${name} filter.`)
        let stringAdd = this.state.argument;
        if (stringAdd != '')
            stringAdd += fieldSeparator;
        stringAdd += (filterSeparator + name);
        this.setState({ argument: stringAdd },
            this.setState({ fullString: this.generateImgLinkEffect(stringAdd) })
        )
    }

    /*
    ** get original image dimension for an image
    */
    onImgLoad = ({target:img}) => {
        this.setState({heightImg:img.offsetHeight,
                       widthImg:img.offsetWidth});
    }

    render() {
        return (
            <div>
                <section className="container-img">
                    <img src={this.state.imageUrl} onLoad={this.onImgLoad} />
                    <img src={this.state.fullString} />
                </section>

                <fieldset className="general-info">
                    <legend>General Information</legend>
                    <div className="container-general-info">
                        <div>
                            <label>Link Img :</label>
                            <input type="text" value={this.state.imageUrl} />
                        </div>
                        <div>
                            <label>Option :</label>
                            <input type="text" value={this.state.argument} />
                        </div>

                        <div>
                            <label>Full string : </label>
                            <input type="text" value={this.state.fullString} />
                        </div>
                    </div>
                </fieldset>

                <fieldset className="image-opt">
                    <legend>Image Dimension</legend>
                    <div>
                        <label>width : </label>
                        <input type="number" min="1" value={this.state.widthImg} onChange={this.handleWidthImg} onBlur={this.handleOnblurDimImg} ></input>
                        <label>height :</label>
                        <input type="number" min="1" value={this.state.heightImg} onChange={this.handleHeightImg} onBlur={this.handleOnblurDimImg}></input>
                        <button onClick={this.updateFullString}>update</button>
                    </div>
                </fieldset>

                <div className="contrainerModifier">
                    <fieldset className="filterFieldset">
                        <legend>Add Filter</legend>
                        <div className="filterButtonContainer">
                            {allFilter.map(filter => <button onClick={() => this.addFilter(filter)}>{filter}</button>)}
                        </div>
                    </fieldset>

                    <fieldset className="textFieldset">
                        <legend>Add text</legend>
                        <section className="text-fieldset-container">
                        <div>
                            <label>text : </label>
                            <input type="text" value={this.state.text} onChange={this.handleText} />
                        </div>

                        <div>
                            <label>pos x : </label>
                            <input type="number" min="1" value={this.state.textPosX} onChange={this.handleTextPosX} />
                            <label>pos y : </label>
                            <input type="number" min="1" value={this.state.textPosY} onChange={this.handleTextPosY} />
                        </div>

                        <div>
                            <label>color : </label>
                            <input type="color" id="head" name="head" value={this.state.textColor} onChange={this.handleTextColor} />
                            <label>size :</label>
                            <input type="number" min="1" max="1000" value={this.state.textSize} onChange={this.handleTextSize} />
                        </div>
                        <div>
                            <button onClick={this.addText}>ADD</button>
                        </div>
                        </section>
                    </fieldset>
                </div>
            </div>
        )
    }
}