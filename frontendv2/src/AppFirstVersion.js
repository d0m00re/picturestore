import React, { Component } from 'react';
import './App.css';

import UploadFile from './Component/UploadFile'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      url: null
    }
  }

  setUrl = (elem) => {
    console.log('SET URL')
    console.log(elem)
    this.setState({ url: elem.url })
  }

  render() {
    return (
      <div className="App">
        <UploadFile setUrl={this.setUrl} />
      </div>
    );
  }
}

export default App;
