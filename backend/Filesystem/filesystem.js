const fs = require('fs');
const fse = require('fs-extra');



//const path = __dirname + '/' + rootPath + '/' + year + '/' + month + '/' + day;

//const path = './' + rootPath + '/' + year + '/' + month + '/' + day;

//const path = './' + rootPath;
/*
console.log(month)
console.log(day)
console.log(year)

console.log(path)

console.log(__dirname)
*/
//fs.mkdirSync(path, {recursive : true}, (err) => {

/*
** make repertories and return path + time
*/

const rootPath = './Image/'

function makeRepertories() {
    const dateObj = new Date();
    const month = dateObj.getUTCMonth() + 1
    const day =  dateObj.getUTCDate();
    const year = dateObj.getUTCFullYear();
    const path = rootPath + year + '/' + month + '/' + day;

    fse.mkdirsSync(path);
    return ({path : path, time : dateObj.getTime()})
}

/*
** return the current path for the file with name
*/
function genPathFile(name){
    const res = makeRepertories();
    console.log(name)
    name = name.replace(/\//g, '_').replace(/-/g, '_')
    const fullPath = res.path + '/' + res.time + '_' + name;
    return fullPath
}

//----------------------------------------------------
// input : 2020-5-13-1589332330113_fa.jpg
// output : ./Image/2020/5/13/1589332330113_fa.jpg
function transformShortPathToFullPath(shortPath){
    let fullpath = rootPath + shortPath.replace(/-/g, '/')

    console.log(shortPath + ' ==> ' + fullpath)

    return (fullpath);
}

// input : ./Image/2020/5/13/1589332330113_fa.jpg
// output : 2020-5-13-1589332330113_fa.jpg
function transformFullPathToShortPath(fullPath){
    let shortPath = fullPath.replace(rootPath, '').replace(/\//g, '-')
    console.log(fullPath + ' --> ' +  shortPath)
    return(shortPath)
}

/*
const test = addFile('ma/bite');
console.log(test)
*/
module.exports = {makeRepertories, genPathFile, transformShortPathToFullPath, transformFullPathToShortPath}