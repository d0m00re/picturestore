const express = require('express')
const app = express()

const cors = require('cors')
const port = process.env.PORT || 3111

/*
** file upload  img
*/
const Formidable = require('formidable')
const bluebird = require('bluebird')
const fs = bluebird.promisifyAll(require('fs'))
const {join} = require('path')
const bodyParser = require('body-parser')

//----------------------------------

// imprt route api
const imgRoute = require('./Route/img')


// route initialisation
app.use('/img/', imgRoute)

app.use(express.json())
app.use(cors())
app.all('*', cors())


//
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
//----

app.get('/', function(req, res){
    res.send('<p>API WORKING</p>');
})

app.listen(port, function(){
    console.log('--> server running on port : ' + port)
})