/*
** manage image storign in our backend server
*/

const express = require('express')
const router = require('express').Router()
const cors = require('cors')
const formidable = require('formidable')
const fs = require('fs')


const { createCanvas, loadImage } = require('canvas')

const { runFilter } = require('../src/genericFilter')

const filesystem = require('../Filesystem/filesystem.js')

const {parsingURL} = require('../src/parsing/parsingUrl')

/*
app.use(cors({
    origin: 'http://yourapp.com'
}));*/
//router.all('*', cors())

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
    next();
});

router.use(express.json())

// upload an image
router.post('/upload', function (req, res) {
    console.log("--- UPLOAD IMAGE ----")
    let url = null

    new formidable.IncomingForm().parse(req)
        .on('fileBegin', (name, file) => {
            file.path = filesystem.genPathFile(file.name)//'./tmp/test.jpg'
            console.log(file.path)
            url = filesystem.transformFullPathToShortPath(file.path)
        })
        .on('file', (name, file) => {
        })
        .on('aborted', () => {
            res.json({ work: false, msg: 'aborted' })
        })
        .on('error', (err) => {
            console.error('Error', err)
            throw err
        })
        .on('end', () => {
            console.log('Success : ' + url)
            res.json({ work: true, msg: 'yhea man', url: url })
        })
})

//./Image/2020/5/13/1589332330113_fa.jpg
//2020-5-13-1589332330113_fa.jpg
router.get('/download/:url', function (req, res) {
    let url = req.params.url
    console.log('get an image : ' + url + ' - ' + filesystem.transformShortPathToFullPath(url))
    url = filesystem.transformShortPathToFullPath(url)
    //  res.sendFile(path, { root: __dirname + '/../' })
    res.sendFile(url, { root: __dirname + '/../' })
})

// exemple for applied somethinh on all pixel
const appliedExemple = (ctx, width, height) => {

    const id = ctx.getImageData(0, 0, width, height);
    ctx.clearRect(0, 0, width, height);
    const data = id.data;

    //applied on all pixel
    const totalValue = [0, 0, 0]
    const totalDiv = 0
    // get current pixel : x * 4 + x * y * 4
    for (let y = 0; y < height; y++) {
        for (let x = 0; x < width; x++) {
            let currentIndex = x * 4 + y * width * 4

            data[currentIndex] = x
            data[currentIndex + 1] = y
            data[currentIndex + 2] = 255

            // console.log(currentIndex)
        }
    }
    ctx.putImageData(id, 0, 0);
}

// Write "Awesome!"
        //t_john,
const drawText = (ctx, text, x = 0, y = 0, size = 30, rotation = 0.0, color = 'blue') => {
             ctx.fillStyle = color
             ctx.font = `${size}px Impact`
             ctx.rotate(rotation)
             ctx.fillText(text, x, y)
             ctx.rotate(-rotation)
}

router.get('/filter/canva/:url', function (req, res) {
    let url = req.params.url
    console.log('get an image : ' + url + ' - ' + filesystem.transformShortPathToFullPath(url))
    url = filesystem.transformShortPathToFullPath(url)
    res.sendFile(url, { root: __dirname + '/../' })
})

//http://backend-service:3111/img/mfilter/canvas/2020-5-13-1589333400909_database_caagru.png/f=sepia(1.0)
router.get('/filter/canva/:url/:modif', function (req, res) {
    const dateObj = new Date();

    let url = filesystem.transformShortPathToFullPath(req.params.url)

    // let parsing = req.params.modfi;
    let parsing = parsingURL(req.params.modif)

    const width = parsing.dim.width
    const height = parsing.dim.height;
    const f = parsing.filter;
    const text = parsing.tabText
    const canvas = createCanvas(width, height)

    const ctx = canvas.getContext('2d')

    loadImage(__dirname + '/../' + url).then((image) => {

        //processing
        ctx.drawImage(image, 0, 0, width, height)
        // applied filter
        for (i in f) {
            runFilter(f[i], ctx, width, height)
        }

        for (i in text){
            drawText(ctx, text[i].text, text[i].x, text[i].y, text[i].size, text[i].rotate, text[i].color)
        }

        //save file
        let tmpFilePath = "/tmp/" + dateObj.getTime() + ".jpeg"
        const out = fs.createWriteStream(__dirname + tmpFilePath)
        const stream = canvas.createJPEGStream()
        stream.pipe(out)
        out.on('finish', () => {
            console.log('Name file created : ' + tmpFilePath)
            res.sendFile(tmpFilePath, { root: __dirname })
        })
    })
        .catch((err) => {
            console.log(err)
            res.json({ error: true })
        })
})

/*
** display json parsing
*/
router.get('/checkParsing/:url/:modif', function (req, res) {
    const dateObj = new Date();

    // let parsing = req.params.modfi;
    let parsing = parsingURL(req.params.modif)

    res.json({ parsing })
})

module.exports = router