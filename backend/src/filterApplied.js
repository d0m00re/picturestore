const { filter } = require('../src/filter')

const appliedFilter = (f, ctx, width, height) => {
    let validFilter = true

    if (typeof f == 'undefined' || f.length == 0) {
        console.log('******************* No FILTER')
        validFilter = false
    }
    else {
        const id = ctx.getImageData(0, 0, width, height);
        ctx.clearRect(0, 0, width, height);
        const data = id.data;
        for (let i = 0; i < data.length; i += 4) {
            let y = filter[f](data[i], data[i + 1], data[i + 2])
            data[i] = y[0];
            data[i + 1] = y[1];
            data[i + 2] = y[2];
        }
        ctx.putImageData(id, 0, 0);
    }
}

module.exports = {appliedFilter : appliedFilter}