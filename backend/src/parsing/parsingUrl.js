const {formatHexToRgb} = require('./formatHexToRgb')
const {isFilter, isRedimH, isRedimW, isText} = require('./is')

// seperate each element like text, or filter
const separator = '|'


function extractGeneric(tab, substr, maxElem = 1, defaultValue = null){
    let elemFind = tab.filter((elem) => {return elem.startsWith(substr, 0)})
    if (elemFind == false || elemFind.length != maxElem)
        return (defaultValue)
    console.log(elemFind)
    let elemProcess = elemFind[0].substring(substr.length)

   return (elemProcess)
}


/*
** substring : begining substring
** array of string
** input : [t_john,x_100,y_100,size_0.0,rgb(150,20,166)], color_
** output : black
** output err : null
*/
function extractTextParam(elem) {
    const subElem = elem.split(',')

    let text =              extractGeneric(subElem, 't_', 1, null)
    let x =    parseInt(    extractGeneric(subElem, 'x_', 1, 0))
    let y =    parseInt(    extractGeneric(subElem, 'y_', 1, 0))
    let size = parseInt(    extractGeneric(subElem, 'size_',1,  30))
    let color =             extractGeneric(subElem, 'color_', 1, 'rgba(0,0,2,1.0)')
    let rotate = parseFloat(extractGeneric(subElem, 'r_', 1, 0.0))

    return ({text, x, y, size, color : formatHexToRgb('#' + color) ,rotate})
}

/*
** f_sepia|w_500|h_500
** http://backend-service:3111/img/checkParsing/2020-5-13-1589333400909_database_caagru.png/f_neg|w_4000|h_4000|c_edge1
*/
function parsingURL(url) {
    // part 1 : parsing 
    let modif = (url.split(separator))

    //parsing filter
    let getFilter = modif.filter(isFilter)

    //parsing dim
    let dimH = modif.filter(isRedimH)
    let dimW = modif.filter(isRedimW)

    // parsing text
    let text = modif.filter(isText)
    let tabText = []
    if (text.length > 0)
    {
        console.log(text)
        //text = extractTextParam(text[0]);
        for (let i in text){
            tabText.push(extractTextParam(text[i]))
        } 
    }
    else
        text = null

    //-------------------

    // extract real dim
    dimH = dimH[0].substring(2)
    dimW = dimW[0].substring(2)

    let tabFilter = []
    if (getFilter.length > 0) {
        for (let i in getFilter)
            tabFilter.push(getFilter[i].substring(2))
    }
    else
        tabFilter = undefined

    //--------------------------------------------

    dimH = parseInt(dimH)
    dimW = parseInt(dimW)

    // part 2 :
    // modifiicaiton pippeline
    return ({ modif: modif, filter: tabFilter, tabText: tabText, dim: { width: dimW, height: dimH } })
}

module.exports = {
    parsingURL : parsingURL
}