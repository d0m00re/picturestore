function isFilter(elem) {
    return elem.includes('f_', 0);
}

function isRedimH(elem) {
    return elem.includes('h_', 0);
}

function isRedimW(elem) {
    return elem.includes('w_', 0);
}
//t_john,x_100,y_100,size_0.0,color_black
function isText(elem) {
    return elem.includes('t_', 0);
}

module.exports = {
    isFilter,
    isRedimH,
    isRedimW,
    isText
}