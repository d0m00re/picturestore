function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }

function formatHexToRgb(hex) {
    let result =hexToRgb(hex)

    if (result == null)
        return `rgba(255, 255, 255, 1.0)`;
    return `rgba(${result.r}, ${result.g}, ${result.b}, 1.0)`;
}

module.exports = {formatHexToRgb : formatHexToRgb}