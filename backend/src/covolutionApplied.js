const { covolutionMatrix } = require('../src/covolutionMatrix')


const getIndex = (x, y, width, height) => {
    return (x * 4 + y * width * 4);
}

const appliedCovolution = (f, ctx, width, height) => {
    console.log('applied convolution')
    const matExp = covolutionMatrix[f]

    console.log('applied covo matrix : ')
    const idRead = ctx.getImageData(0, 0, width, height);
    const idWrite = ctx.getImageData(0, 0, width, height);
    ctx.clearRect(0, 0, width, height);
    const dataRead = idRead.data;
    const dataWrite = idWrite.data;

    let totalSum = [0, 0, 0]
    // substract index
    let substractIndex = Math.floor(matExp.size / 2)
    console.log(substractIndex)

    // get current pixel : x * 4 + x * y * 4
    for (let y = 0; y < height; y++) {
        for (let x = 0; x < width; x++) {
            let currentIndex = getIndex(x, y, width, height)//x * 4 + y * width * 4
            // futur refactir
            totalSum = [0, 0, 0]
            let actualIndex = 0
            // matrice pixel calculator
            for (let yy = 0; yy < matExp.size; yy++) {
                for (let xx = 0; xx < matExp.size; xx++) {
                    actualIndex = getIndex(x + xx - substractIndex, y + yy - substractIndex, width, height)
                    //actualIndex = getIndex(x + xx - 1, y + yy - 1, width, height)
                    totalSum[0] += (dataRead[actualIndex] * matExp.matrix[yy * matExp.size + xx])
                    totalSum[1] += (dataRead[actualIndex + 1] * matExp.matrix[yy * matExp.size + xx])
                    totalSum[2] += (dataRead[actualIndex + 2] * matExp.matrix[yy * matExp.size + xx])
                }
            }
            // * factor
            totalSum[0] *= matExp.factor;
            totalSum[1] *= matExp.factor;
            totalSum[2] *= matExp.factor;

            // update new canva data
            dataWrite[currentIndex] = totalSum[0]
            dataWrite[currentIndex + 1] = totalSum[1]
            dataWrite[currentIndex + 2] = totalSum[2]
        }
    }
    ctx.putImageData(idWrite, 0, 0);
}

module.exports = {appliedCovolution : appliedCovolution}