function filterGrayscaleValue(r, g, b) {
    let y = 0.299 * r + 0.587 * g + 0.114 * b;

    return ([y, y, y])
}

function filterSepiaValue(r, g, b) {
    return ([
        Math.min(0.393 * r + 0.769 * g + 0.189 * b, 255),
        Math.min(0.349 * r + 0.686 * g + 0.168 * b, 255),
        Math.min(0.272 * r + 0.534 * g + 0.131 * b, 255)
    ])
}

function filterNegValue(r, g, b) {
    return ([
        255 - r,
        255 - g,
        255 - b
    ])
}

function filterRedValue(r, g, b) {
    return ([
        r,
        0,
        0
    ])
}

function filterGreenValue(r, g, b) {
    return ([
        0,
        g,
        0
    ])
}

function filterBlueValue(r, g, b) {
    return ([
        0,
        0,
        b
    ])
}

let filter = {
    gray: filterGrayscaleValue,
    sepia: filterSepiaValue,
    neg: filterNegValue,
    red: filterRedValue,
    blue: filterBlueValue,
    green: filterGreenValue
}

module.exports = {filter : filter}