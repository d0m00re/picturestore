const gaussianBlur = {
    name: 'gaussianBlur',
    matrix: [1, 2, 1,
        2, 4, 2,
        1, 2, 1],
    factor: 0.0625,
    size: 3
}

const exemple = {
    name: 'exemple',
    matrix: [
        1, 1, 1,
        1, 1, 1,
        1, 1, -8
    ],
    factor: 1.0,
    size: 3
}

const identity = {
    name: 'identity',
    matrix: [
        0, 0, 0,
        0, 1, 0,
        0, 0, 0
    ],
    factor: 1.0,
    size: 3
}

const edge1 = {
    name: 'edge1',
    matrix: [
        1, 0, -1,
        0, 0, 0,
        -1, 0, 1
    ],
    factor: 1.0,
    size: 3
}

const edge2 = {
    name: 'edge2',
    matrix: [
        0, 1, 0,
        1, -4, 1,
        0, 1, 0
    ],
    factor: 1.0,
    size: 3
}

const edge3 = {
    name: 'edge3',
    matrix: [
        -1, -1, -1,
        -1, 8, -1,
        -1, -1, -1
    ],
    factor: 1.0,
    size: 3
}

const sharpen ={
    name : 'sharpen',
    matrix : [
        0, -1, 0,
        -1, 5, -1,
        0, -1, 0
    ],
    factor : 1.0,
    size: 3
}

const boxBlur = {
    name : 'boxBlur',
    matrix : [
        1, 1, 1,
        1, 1, 1,
        1, 1, 1 
    ],
    factor : 0.11111111111,
    size : 3
}

const gaussianBlur2 = {
    name : 'gaussianBlur2',
    matrix : [
        1, 4,  6,  4,  1,
        4, 16, 24, 16, 4,
        6, 24, 36, 24, 6,
        4, 16, 24, 16, 4,
        1, 4,  6,  4,  1
    ],
    factor : 1 / 256,
    size : 5
}

const unsharpMasking = {
    name : "unsharpMasking",
    matrix : [
        1, 4, 6, 4, 1,
        4, 16, 24, 16, 4,
        6, 24, -476, 24, 6,
        4, 16, 24, 16, 4,
        1, 4, 6, 4, 1
    ],
    factor : -1 / 256,
    size : 5
}

const convM = {
    'exemple': exemple,
    'identity' : identity,
    'gaussianBlur': gaussianBlur,
    'gaussianBlur2' : gaussianBlur2,
    'edge1' : edge1,
    'edge2' : edge2,
    'edge3' : edge3,
    'sharpen' : sharpen,
    'boxBlur' : boxBlur,
    'unsharpMasking' : unsharpMasking
}

module.exports = {
    covolutionMatrix : convM
}